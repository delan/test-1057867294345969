# How to keep a production branch behind master

## problem

* say we have two branches, master and production
* to deploy, we merge (--no-ff) master into production
* Bitbucket now says production is way ahead of master
* we don't want production to be ahead of master
    * (unless we have to do a hotfix or similar)

## possible solution

* when deploying, merge with --ff-only (or at least try, with --ff)
    * or set git config merge.ff and/or pull.ff to only (or true)
    * or equivalent, if you don't actually deploy with git-merge(1)

## why?

* think of a git branch as a directed graph from its head commit
    * each commit points to its parent (or parents, for merge commits)
* most of the "ahead" commits are probably your deployment merge commits
    * "13 ahead" means production can reach 13 commits that master can't
    * "13 behind" means master can reach 13 commits that production can't
* "fast-forward" merging is possible if production is reachable from master
    * as in production's head commit is an ancestor of master's head commit
    * given the scenario in question, this is true almost all of the time
        * exception: if you have hotfixes not yet merged back to master

## try me!

    rm -Rf .git                               # (no repository)
    git init                                  # master     (no HEAD)
    git commit --allow-empty -m a             # master     (HEAD: a)
    git commit --allow-empty -m b             # master     (HEAD: b)
    git commit --allow-empty -m c             # master     (HEAD: c)
    git commit --allow-empty -m d             # master     (HEAD: d)
    git commit --allow-empty -m e             # master     (HEAD: e)
    git commit --allow-empty -m f             # master     (HEAD: f)
    git commit --allow-empty -m g             # master     (HEAD: g)
    git checkout -b production                # production (HEAD: g)
    git checkout master                       # master     (HEAD: g)
    git commit --allow-empty -m h             # master     (HEAD: h)
    git commit --allow-empty -m i             # master     (HEAD: i)
    git commit --allow-empty -m j             # master     (HEAD: j)
    git commit --allow-empty -m k             # master     (HEAD: k)
    git commit --allow-empty -m l             # master     (HEAD: l)
    git commit --allow-empty -m m             # master     (HEAD: m)
    git checkout production                   # production (HEAD: g)
    git merge --no-ff --no-edit master        # production (HEAD: merge(g,m))
    git commit --allow-empty -m fix           # production (HEAD: fix)
    git checkout master                       # master     (HEAD: m)
    git commit --allow-empty -m n             # master     (HEAD: n)
    git commit --allow-empty -m o             # master     (HEAD: o)
    git commit --allow-empty -m p             # master     (HEAD: p)
    git commit --allow-empty -m q             # master     (HEAD: q)
    git merge --no-ff --no-edit production    # master     (HEAD: merge(q,fix))
    git commit --allow-empty -m r             # master     (HEAD: r)
    git commit --allow-empty -m s             # master     (HEAD: s)
    git add README.md                         # master     (HEAD: s)
    git commit -m README.md                   # master     (HEAD: README.md)
    git checkout production                   # production (HEAD: fix)
    git merge --ff-only master                # production (HEAD: README.md)
